
const pathForDetails = `http://www.omdbapi.com/?apikey=4b601aab&i=`;

export default class FilmService {
  static getFilms = async (pageNumber, query = 'react', year) => {
    try {
      const res = await fetch(`http://www.omdbapi.com/?apikey=4b601aab&s=*${query}*&y=${year.toString()}&type=movie&page=${pageNumber}`);
      return await res.json();
    } catch (e) {
      return "error";
    }
  };
  static getSelectedFilm = async itemId => {
    try {
      const res = await fetch(pathForDetails + itemId);
      return await res.json();
    } catch (e) {
      return "error";
    }
  };

}
