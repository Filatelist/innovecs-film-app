import React, { useState } from "react";
import DetailsPage from "./components/details-page";
import MainPage from "./components/main-page";
import Header from "./components/header";
import ErrorProvider from "./components/error-provider";
import FilmService from "./filmService";
import { connect } from "react-redux";
import { setFilm } from "./actions";
import "./App.css";

function App({ currentFilm, setFilm }) {
  const [query, setQuery] = useState("");
  const [year, setYear] = useState("");

  const onGetFilm = id => {
    FilmService.getSelectedFilm(id).then(body => {
      if (body === "error") {
        return <ErrorProvider />;
      }
      setFilm(body);
    });
  };

  const onSearchInputChanged = event => {
    const query = event.target.value;
    setQuery(event.target.value);
  };

  const onYearSelected = event => {
    setYear(event.target.value);
  };

  return (
    <div className="App">
      <Header
        query={query}
        year={year}
        onSearch={onSearchInputChanged}
        onSelectYear={onYearSelected}
      />
      {currentFilm !== "" && (
        <DetailsPage
          currentFilm={currentFilm}
          exitDetails={() => setFilm("")}
        />
      )}
      <MainPage query={query} year={year} getFilm={onGetFilm} />
    </div>
  );
}

const mapStateToProps = ({ currentFilm }) => {
  return {
    currentFilm
  };
};

const mapDispatchToProps = {
  setFilm
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
