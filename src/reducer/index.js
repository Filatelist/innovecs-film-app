const initialState ={
    filmList: [],
    currentFilm: ""
};

const reducer = (state = initialState, action) =>{
    switch(action.type){
        case "GET_ALL_FILMS":
            return {
                ...state,
                filmList: action.payload.Search
            };
        case "GET_CURRENT_FILM":

            return {
                ...state,
                currentFilm: action.payload
            };
        default: return state;
    }
};

export default reducer;
