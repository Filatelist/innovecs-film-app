import FilmService from "../filmService";

import {GET_CURRENT_FILM, GET_ALL_FILMS} from './constants';

const getAllFilms = (page, searchQuery, year) => (next) =>{
    FilmService.getFilms(page, searchQuery, year).then((body)=>{
        next({
            type: GET_ALL_FILMS,
            payload: body
        })
    })
};

const setFilm = (body) =>{
    return {
        type: GET_CURRENT_FILM,
        payload: body
    }
};


export {getAllFilms, setFilm}