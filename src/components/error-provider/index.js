import React from 'react';

const ErrorProvider = () =>{
    return(<div className="container text-align-center">
        <h1>Error something goes wrong</h1>
        <p>CHECK YOUR INTERNET CONNECTION OR </p>
        <p>Wait until the API will be recover</p>
    </div>);
};
export default ErrorProvider;