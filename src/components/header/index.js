import React  from "react";
import "bootstrap/dist/css/bootstrap.css";

const Header = ({ onSearch, onSelectYear, query, year }) => {
  let options = [];
  for (let i = 1990; i <= 2019; i++) {
    options.push(i);
  }
  return (
    <nav className="navbar sticky-top navbar-expand-lg navbar-dark bg-dark">
      <div className="navbar-brand d-flex" href="#">
        Year:
          <div className="mr-5">
              <select name="year" onChange={onSelectYear} value={year} id="selectYear">
                  <option key={"0"} value=""> </option>
                  {options.map(item => {
                      return(
                          <option key={item}  value={item}>{item}</option>
                      )})}
              </select>
          </div>

      </div>
      <form className="form-inline float-right" onSubmit={(event)=>{event.preventDefault()}}>
        <input
          type="text"
          className="mr-sm-2  form-control"
          onChange={onSearch}
          placeholder="Type to search Film"
          value={query}
        />
      </form>
    </nav>
  );
};
export default Header;
