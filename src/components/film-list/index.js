import React from "react";
import FilmCard from "../film-card";

const FilmList = ({ updatedList = [], getFilm }) => {
  console.log('render')
  if (!updatedList[0]) {
    return (
      <div className="container color-text w-100 text-align-center">
        <h2>Type to search</h2>
      </div>
    );
  }
  return (
    <div className="container d-flex">
      {updatedList.map(({ Poster, Title, Type, Year, imdbID }, index) => {
          let key = Math.random() * 9999999999999;

        return (
          <FilmCard
            key={key+index}
            imdbID={imdbID}
            Poster={Poster}
            Title={Title}
            Type={Type}
            Year={Year}
            receiveFilm={getFilm}
          />
        );
      })}
    </div>
  );
};

export default FilmList;
