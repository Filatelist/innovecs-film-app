import React from 'react';

const Paginator =({onChangePage, page})=>{
    return(
        <div className="w-100 shadow-sm text-align-center  pt-2 pb-2 bg-light">
            <div className={"mr-auto "}>
                <button
                    id="1"
                    className={`btn-page ${page === 1 ? "marked" : ""}`}
                    onClick={onChangePage}
                >
                    1
                </button>
                <button
                    id="2"
                    className={`btn-page ${page === 2 ? "marked" : ""}`}
                    onClick={onChangePage}
                >
                    2
                </button>
            </div>
        </div>
    );
};
export default Paginator