import React, { useState, useEffect, useMemo } from "react";
import FilmList from "../film-list";
import LoadingIndicator from "../loading-indicator";
import Paginator from "./paginator";
import "bootstrap/dist/css/bootstrap.css";
import "./main-page.css";
import { connect } from "react-redux";
import { getAllFilms } from "../../actions";
import ErrorProvider from "../error-provider";
import PropTypes from 'prop-types';

const MainPage = ({ getFilm, query, year, filmList, getAllFilms }) => {
  const [page, setPage] = useState(1);
  const [isError, setError] = useState(false);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getAllFilms(page, query, year);
    setLoading(false);
  }, [page, query, year]);

  const onChangePage = event => {
    setPage(+event.target.id);
  };
  const memoizedFilmList = useMemo(() => {
    return <FilmList getFilm={getFilm} updatedList={filmList} />;
  }, [filmList, getFilm]);

  if (isError) {
    return <ErrorProvider />;
  }
  if (loading) {
    return (
      <>
        <LoadingIndicator />
      </>
    );
  }

  return (
    <>
      <Paginator onChangePage={onChangePage} page={page} />
      {memoizedFilmList}
    </>
  );
};

MainPage.propTypes = {
  query: PropTypes.string.isRequired,
  filmList: PropTypes.arrayOf(PropTypes.object),
  getAllFilms: PropTypes.func.isRequired,
  getFilm: PropTypes.func.isRequired
};

const mapStateToProps = ({ filmList }) => {
  return {
    filmList
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getAllFilms: (page, query, year) => dispatch(getAllFilms(page, query, year))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainPage);
