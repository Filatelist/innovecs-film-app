import React from 'react';

import 'bootstrap/dist/css/bootstrap.css'
import './film-card.css'
const FilmCard = ({Poster, Title, Type, Year, imdbID,receiveFilm}) =>{
    return (
        <div className="d-block card shadow-sm bg-light">
            <div className="img" onClick={() => receiveFilm(imdbID)}>
                    {
                        Poster === "N/A"?<div className="without-img color-text"><h2>No Image</h2></div>: <img src={Poster} alt="poster"/>
                    }
            </div>
            <div className="info pl-5 pt-2">
                <ul className="list-group">
                    <li className=""><h6>{Title}</h6></li>
                    <li className=""><h6>{Type}</h6></li>
                    <li className="color-text"><h6>{Year}</h6></li>
                </ul>
                <button onClick={() => receiveFilm(imdbID)} className="btn btn-default">READ MORE ...</button>
            </div>
        </div>
    );
};

export default FilmCard