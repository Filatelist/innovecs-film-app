import React from 'react';

const LoadingIndicator = () =>{
    return(
        <div className="w-100 text-align-center color-text mt-2">
            <h3>Loading...</h3>
        </div>
    );
};

export default LoadingIndicator