import React from "react";
import "bootstrap/dist/css/bootstrap.css";
import "./details-page.css";
import cross from "./error.svg";
const DetailsPage = ({ currentFilm, exitDetails }) => {

  const {
    Poster,
    Title,
    Director,
    Actors,
    Released,
    BoxOffice,
    Country,
    Genre,
    Runtime,
    Plot,
    imdbRating,
    imdbVotes
  } = currentFilm;

  return (
    <div className="row w-75 shadow-lg pt-4 the-film pb-5">
      <div className=" w-100">
        <span className="float-right p-2" onClick={exitDetails}>
          <img width="30" src={cross} alt="" />
        </span>
      </div>
      <div className="pt-5 ">
        <img width="150" className="ml-2 col-lg-4" src={Poster} alt="" />
        <ul className="list-group f-d-normal w-50 col-lg-8 float-right">
          <li>{Title}</li>
          <li>DIRECTOR: {Director}</li>
          <li>CAST: {Actors}</li>
          <li>YEAR: {Released}</li>
          <li>MONEY: {BoxOffice}</li>
          <li>COUNTRY: {Country}</li>
          <li>GENRE: {Genre}</li>
          <li>RUNTIME: {Runtime}</li>
          <li>PLOT: {Plot}</li>
          <li>Rate: {imdbRating}</li>
          <li>Votes: {imdbVotes}</li>
        </ul>
      </div>
    </div>
  );
};
export default DetailsPage;
